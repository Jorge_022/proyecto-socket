package com.example.practicasocket;

import java.io.*;
import java.net.*;
import java.util.concurrent.*;

public class CalculadoraServer {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(5);
        try (ServerSocket serverSocket = new ServerSocket(50005)) {
            System.out.println("Servidor escuchando en el puerto 50005...");
            while (true) {
                pool.execute(new ClientHandler(serverSocket.accept()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
        }
    }

    private static class ClientHandler implements Runnable {
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;

        public ClientHandler(Socket socket) {
            this.clientSocket = socket;
        }

        @Override
        public void run() {
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    if ("salir".equals(inputLine)) {
                        break;
                    }
                    double var1 = Double.parseDouble(inputLine);
                    out.println("Dime el segundo operando");
                    double var2 = Double.parseDouble(in.readLine());
                    out.println("Dime el operador [+,-,*,/]");
                    String operator = in.readLine();
                    double result = 0;
                    switch (operator) {
                        case "+":
                            result = var1 + var2;
                            break;
                        case "-":
                            result = var1 - var2;
                            break;
                        case "*":
                            result = var1 * var2;
                            break;
                        case "/":
                            if (var2 != 0) {
                                result = var1 / var2;
                            } else {
                                out.println("Error: División por cero");
                            }
                            break;
                        default:
                            out.println("Operador no válido");
                            break;
                    }
                    out.println(result);
                }
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
