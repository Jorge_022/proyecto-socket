package com.example.practicasocket;

import java.io.*;
import java.net.*;

public class ServidorDatagramas {
    public static void main(String[] args) {
        try (DatagramSocket serverSocket = new DatagramSocket(50005)) {
            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];
            System.out.println("Servidor escuchando en el puerto 50005...");
            while (true) {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);
                String inputLine = new String(receivePacket.getData(), 0, receivePacket.getLength());
                if ("salir".equals(inputLine)) {
                    break;
                }
                double var1 = Double.parseDouble(inputLine);

                // Recibiendo segundo operando
                receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);
                inputLine = new String(receivePacket.getData(), 0, receivePacket.getLength());
                double var2 = Double.parseDouble(inputLine);

                // Recibiendo operador
                receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);
                inputLine = new String(receivePacket.getData(), 0, receivePacket.getLength());
                String operator = inputLine.trim();

                double result = 0;
                switch (operator) {
                    case "+":
                        result = var1 + var2;
                        break;
                    case "-":
                        result = var1 - var2;
                        break;
                    case "*":
                        result = var1 * var2;
                        break;
                    case "/":
                        if (var2 != 0) {
                            result = var1 / var2;
                        } else {
                            result = Double.POSITIVE_INFINITY;
                        }
                        break;
                    default:
                        result = Double.NaN;
                        break;
                }

                String response = Double.toString(result);
                sendData = response.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, receivePacket.getAddress(), receivePacket.getPort());
                serverSocket.send(sendPacket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
