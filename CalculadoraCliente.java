package com.example.practicasocket;

import java.io.*;
import java.net.*;

public class CalculadoraCliente {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 50005);
             PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {

            String userInput;
            while ((userInput = stdIn.readLine()) != null) {
                out.println(userInput);
                System.out.println("Servidor: " + in.readLine());
            }
        } catch (UnknownHostException e) {
            System.err.println("No puedo encontrar el host localhost");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("No puedo conectar con localhost en el puerto 50005");
            System.exit(1);
        }
    }
}
