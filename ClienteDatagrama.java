package com.example.practicasocket;

import java.io.*;
import java.net.*;

public class ClienteDatagrama {
    public static void main(String[] args) {
        try (DatagramSocket clientSocket = new DatagramSocket();
             BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {

            InetAddress IPAddress = InetAddress.getByName("localhost");
            byte[] sendData;
            byte[] receiveData = new byte[1024];
            String userInput;

            while ((userInput = stdIn.readLine()) != null) {
                sendData = userInput.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 50005);
                clientSocket.send(sendPacket);

                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);
                String response = new String(receivePacket.getData(), 0, receivePacket.getLength());
                System.out.println("Servidor: " + response);
            }
        } catch (UnknownHostException e) {
            System.err.println("No puedo encontrar el host localhost");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Error de E/S");
            System.exit(1);
        }
    }
}
